# avia-search-results

[Тестовое задание](./test-instructions/test-instructions.md) со страницей результатов поиска полетов.

[Деплой](https://avia-search-results.netlify.app/).

## Реализовано

* Сортировка по цене и времени
* Динамически формирующиеся чекбоксы для фильтров по пересадкам и компаниям
* Сами фильтры (цена, пересадки, компании)
* Динамически формирующиеся карточки с вылетами

## Стэк

* React
* Typescript
* CSS Modules
* SCSS
* [uuidv4](https://github.com/uuidjs/uuid)
* [use-debounce](https://github.com/xnimorz/use-debounce)

## Установка

`npm i  
npm start`
